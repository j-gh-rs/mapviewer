package tree;

import java.util.ArrayList;
import java.util.Collections;

import gridpane.ImageTile;

public class Node<A> {

	private final Node<A> parent;
	private final int depth;
	private final ArrayList<Node<A>> children;
	private A a;
	
	public Node() {
		this(null, 0);
	}
	
	public Node(Node<A> parent) {
		this(parent, parent.depth + 1);
	}
	
	public Node(Node<A> parent, int depth) {
		this.parent = parent;
		this.depth = depth;
		children = new ArrayList<Node<A>>(Collections.nCopies(4, null));
		for (int i = 0; i < 4; i++)
			children.add(null);
	}

	public A get() {
		return a;
	}

	public void set(A a) {
		this.a = a;
	}

	public Node<A> get(int i, int j) {
		int idx = (i << 1) | j;
		if (children.get(idx) == null)
			children.set(idx, new Node<A>(this));
		return children.get(idx);
	}

	public Node<A> get(int depth, int i, int j) {

//		Node<A> node = get(i >> (depth - this.depth - 1), j >> (depth - this.depth - 1));
//		(i & ((1 << (depth - this.depth)) - 1)) >> (depth - this.depth - 1)
		
		Node<A> node = get((i & ((1 << (depth - this.depth)) - 1)) >> (depth - this.depth - 1), (j & ((1 << (depth - this.depth)) - 1)) >> (depth - this.depth - 1));
		return depth == this.depth + 1 ? node : node.get(depth, i, j);
	}

}
