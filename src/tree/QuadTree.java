package tree;

public class QuadTree<A> {
	
	private final Node<A> root;
	
	public QuadTree() {
		root = new Node<A>();
	}

	public Node<A> getRoot() {
		return root;
	}

	public Node<A> get(int depth, int i, int j) {
		if (depth == 0)
			return root;
		return root.get(depth, i, j);
	}

}
