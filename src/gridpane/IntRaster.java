package gridpane;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.Raster;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class IntRaster {
	
	public final int[] data;
	public final int width;
	public final int height;
	
	public IntRaster(int width, int height) {
		this.data = new int[width*height];
		this.width = width;
		this.height = height;
	}
	
	public IntRaster(int[] data, int width) {
		this.data = data;
		this.width = width;
		this.height = data.length / width;
	}
	
	/** Pastes a raster in this raster at location x, y, cropping where necessary */
	public void draw(IntRaster src, int x, int y) {
		if (x > width || y > height || x + src.width < 0 || y + src.height < 0)
			return;
		
		int idx_src = Math.max(-y, 0) * src.width + Math.max(-x, 0);
		int idx_dst = Math.max(y, 0) * width + Math.max(x, 0);
		
		int d_src = Math.max(-x, 0) + Math.max(src.width - width + x, 0);
		int d_dst = Math.max(x, 0) + Math.max(-src.width + width - x, 0);
		
		while (idx_src < src.data.length && idx_dst < data.length) {
			for (int i = 0; i < src.width - d_src; i++) {
				if (src.data[idx_src] != 0)
					data[idx_dst] = src.data[idx_src];
				idx_dst++;
				idx_src++;
			}
			idx_dst += d_dst;
			idx_src += d_src;
		}
	}
	
	public void export() {
//		SinglePixelPackedSampleModel sm = new SinglePixelPackedSampleModel(DataBuffer.TYPE_INT, width, height, new int[]{0xFF0000, 0xFF00, 0xFF, 0xFF000000});
//		DataBufferInt db = new DataBufferInt(data, data.length);
//		WritableRaster wr = Raster.createWritableRaster(sm, db, null);
//		BufferedImage image = new BufferedImage(ColorModel.getRGBdefault(), wr, false, null);
		
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster raster = (WritableRaster) image.getRaster();
		raster.setPixels(0, 0, width, height, data);
		try {
			ImageIO.write(image, "png", new File(System.currentTimeMillis()+".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
