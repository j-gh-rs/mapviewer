package gridpane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Canvas {
	
	public int width;
	public int height;
	
	private boolean transparent = false;
	private BufferedImage buffer;
	public int[] pixels;
	public IntRaster raster;
	public Graphics2D g2d;

	public Canvas(int width, int height, boolean transparent) {
		this.transparent = transparent;
		updateSize(width, height);
	}
	
	public Canvas() {
	}

	public void updateSize(int w, int h) {
		if (w != width || h != height) {
			width = w;
			height = h;
			buffer = new BufferedImage(width, height, transparent ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB);
			pixels = ((DataBufferInt)buffer.getRaster().getDataBuffer()).getData();
			raster = new IntRaster(pixels, width);
			g2d = (Graphics2D)buffer.getGraphics();
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setFont(new Font("default", Font.BOLD, 12));
		}
	}
	
	public void clear() {
		if (pixels == null)
			return;
		for (int i = 0 ; i < pixels.length; i++)
			pixels[i] = 0;
	}

	public void draw(Graphics g) {
		g.drawImage(buffer, 0, 0, null);
	}

	public void export() {
		export(new File(System.currentTimeMillis()+".png"));
	}

	public void export(File file) {
		try {
			ImageIO.write(buffer, "png", file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void draw(IntRaster raster, int x, int y) {
		this.raster.draw(raster, x, y);
	}
	
	public void drawLine(int c, int x0, int y0, int x1, int y1) {
		drawLine(new Color(c), x0, y0, x1, y1);
	}
	
	public void drawLine(Color c, int x0, int y0, int x1, int y1) {
		g2d.setColor(c);
		g2d.drawLine(x0, y0, x1, y1);
	}

}
