package gridpane;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import layers.Layer;

public class GridPane extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener {

	private static final Color DARK_GRAY = new Color(80, 80, 80);

	private static final int TILE_SCALE = 8;
	public static final int TILE_SIZE = 1 << TILE_SCALE;
	
	public static GridPane INSTANCE;
	
	private final Canvas canvas = new Canvas();
	
	private final ArrayList<Layer> layers = new ArrayList<Layer>();

	public int scale = 0;
	public int xs, ys;

	private int xp, yp; /** mouse press location */
	private int xd, yd; /** drag location */

	private int xmouse, ymouse; /** mouse location */
	
	public GridPane() {
		INSTANCE = this;
		
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addMouseWheelListener(this);
		
		Action resetAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				resetView();
				repaint();
			}
		};
		
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), "Reset");
		getActionMap().put("Reset", resetAction);
		
//		Action f2Action = new AbstractAction() {
//			public void actionPerformed(ActionEvent e) {
//				xs = 1<<scale;
//				repaint();
//			}
//		};
//		
//		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0), "f2");
//		getActionMap().put("f2", f2Action);
	}
	
	public void addLayer(Layer layer) {
		layers.add(layer);
	}
	
	void update() {
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		if (getWidth() <= 0 || getHeight() <= 0)
			return;
		canvas.updateSize(getWidth(), getHeight());
		paintBuffer();
		canvas.draw(g);
	}
	
	private void paintBuffer() {
		canvas.clear();
		
		int dy = -ys + yd;
		int dx = -xs + xd;
		
		int x, y;

		int iMin = - ((canvas.height - dy) >> TILE_SCALE);
		int iMax = (dy + TILE_SIZE - 1) >> TILE_SCALE;
		int jMin = - ((dx + TILE_SIZE - 1) >> TILE_SCALE);
		int jMax = (canvas.width - dx) >> TILE_SCALE;

		for (int i = iMin; i <= iMax; i++) {
			y = dy - i * TILE_SIZE;
			for (int j = jMin; j <= jMax; j++) {
				x = dx + j * TILE_SIZE;
				canvas.drawLine(DARK_GRAY, x, y, x+TILE_SIZE, y);
				canvas.drawLine(DARK_GRAY, x, y, x, y+TILE_SIZE);
				canvas.g2d.drawString(j+","+i, x+(TILE_SIZE>>1), y+(TILE_SIZE>>1));
			}
		}

		for (Layer layer : layers) {
			layer.draw(canvas, scale, dx, dy, iMin, iMax, jMin, jMax);
		}

		canvas.g2d.setColor(DARK_GRAY);
		canvas.g2d.drawString(strjoin(", ", dx, dy, scale, iMin, iMax, jMin, jMax), xmouse, ymouse);
		
//		for (int i = (-ys + yd) & (d - 1); i < canvas.height; i += d) {
//			for (int j = (-xs + xd) & (d - 1); j < canvas.width; j += d) {
//				for (int k = 0; k < d; k++) {
//					if (j+k < canvas.width)
//						canvas.pixels[i * canvas.width + j + k] = 0xAAAAAA;
//					if (i+k < canvas.height)
//						canvas.pixels[(i+k) * canvas.width + j] = 0xAAAAAA;
//				}
//				canvas.g2d.drawString((xs - xd + j >> 8)+","+(ys - yd + i >> 8), j+(d>>1), i+(d>>1));
////				canvas.g2d.drawString((xs - xd + j >> scale)+","+(ys - yd + i >> scale), j, i);
//			}
//		}
		
		
//		/** draw mouse crosshair */
//		canvas.g2d.setColor(Color.GRAY);
//		canvas.g2d.drawLine(0, (mouseSnappedLocation.y << scale) - (ys - yd), getWidth(), (mouseSnappedLocation.y << scale) - (ys - yd));
//		canvas.g2d.drawLine((mouseSnappedLocation.x << scale) - (xs - xd), 0, (mouseSnappedLocation.x << scale) - (xs - xd), getHeight());
		
	}

	public static String strjoin(String delimiter, Object...objs) {
		String[] strs = new String[objs.length];
		for (int i = 0; i < objs.length; i++)
			strs[i] = String.valueOf(objs[i]);
		return String.join(delimiter, strs);
	}

	public void drawTile(int i, int j) {
		
	}

//	public void fillRect(int i, int j, int color) {
//		int d = 1 << scale;
//		
//		int yMin = i*d - ys + yd;
//		int yMax = Math.min(canvas.height, yMin + d);
//		yMin = Math.max(0, yMin);
//		int xMin = j*d - xs + xd;
//		int xMax = Math.min(canvas.width, xMin + d);
//		xMin = Math.max(0, xMin);
//		
//		for (int y = yMin; y < yMax; y++) {
//			for (int x = xMin; x < xMax; x++) {
//				canvas.pixels[y * canvas.width + x] = color;
//			}
//		}
//	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			xd = e.getX() - xp;
			yd = e.getY() - yp;
		}
		repaint();
	}

	@Override public void mouseClicked(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {
		this.xmouse = e.getX();
		this.ymouse = e.getY();
		repaint();
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			xp = e.getX();
			yp = e.getY();
		}
		repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			xs -= e.getX() - xp;
			ys -= e.getY() - yp;
			xd = 0;
			yd = 0;
		}
		repaint();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.getWheelRotation() > 0) {
			if (scale <= 0)
				return;
			xs = ((e.getX() + xs) >> 1) - e.getX();
			ys = ((e.getY() + ys + 256) >> 1) - e.getY();
		} else {
			if (scale >= 14)
				return;
			xs = ((e.getX() + xs) << 1) - e.getX();
			ys = ((e.getY() + ys - 128) << 1) - e.getY();
		}

		scale += e.getWheelRotation() > 0 ? -1 : 1;
		
		repaint();
	}

	public void resetView() {
		xs = 0;
		ys = 0;
		scale = 0;
		repaint();
	}

	public static final class Rectangle {
		public int xMin, xMax, yMin, yMax;
		
		private Rectangle(int xMin, int xMax, int yMin, int yMax) {
			this.xMin = xMin;
			this.xMax = xMax;
			this.yMin = yMin;
			this.yMax = yMax;
		}
	}
}
