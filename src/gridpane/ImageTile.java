package gridpane;

import java.awt.Color;

public class ImageTile extends IntRaster {
	
	public static final ImageTile EMPTY_TILE = new ImageTile();

	public ImageTile() {
		super(GridPane.TILE_SIZE, GridPane.TILE_SIZE);
	}

	public void drawRect(int x, int y, int width, int height, int color) {
		for (int y_ = y; y_ < y + height; y_++) {
			data[(y_ << 8) | x] = color;
			data[(y_ << 8) | (x+width-1)] = color;
		}
		for (int x_ = x; x_ < x + width; x_++) {
			data[(y << 8) | x_] = color;
			data[((y+height-1) << 8) | x_] = color;
		}
	}

	public void fillRect(int x, int y, int width, int height, int color) {
		for (int y_ = y; y_ < y + height; y_++)
			for (int x_ = x; x_ < x + width; x_++)
				data[(y_ << 8) | x_] = color;
	}

	public void drawHorizontalLine(int x, int y, int width, int color) {
		for (int x_ = x; x_ < x + width; x_++)
			data[(y << 8) | x_] = color;
	}

	public void drawVerticalLine(int x, int y, int height, int color) {
		for (int y_ = y; y_ < y + height; y_++)
			data[(y_ << 8) | x] = color;
	}
}
