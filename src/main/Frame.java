package main;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;

import javax.swing.JFrame;

import gridpane.GridPane;
import layers.clip.ClipLayer;
import layers.region.RegionLayer;
import shared.io.Buffer;

public class Frame extends JFrame {
	
	public static final File DATA_PATH = new File("/media/guestaccount/Samsung/linux/git/2006Redone/Server/data");
	
	Frame() {
//		JSplitPane spLeft = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JLabel("Map settings"), new JLabel("Tile properties"));
//		JSplitPane spRight = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JLabel("3D view"), new GridPane() {
//			@Override
//			public void drawTile(int i, int j) {
//				Map.drawTile(this, i, j);
//			}
//		});
//		JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, spLeft, spRight);
		
		GridPane sp = new GridPane();
		sp.addLayer(new RegionLayer());
		sp.addLayer(new ClipLayer());
		
		sp.setPreferredSize(new Dimension(400, 400));
		
		super.getContentPane().setLayout(new BorderLayout());
		super.getContentPane().add(sp, BorderLayout.CENTER);
		
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.pack();
//		super.setSize(400, 400);
		super.setLocationRelativeTo(null);
		super.setVisible(true);
	}
	
	public static void main(String[] args) {
		
//		Buffer buffer = new Buffer();
//		
//		buffer.writeInt16((0 << 8) | 0);
//		buffer.writeInt32(0xFF);
//		buffer.writeInt8(0);
//
//		buffer.writeInt16((255 << 8) | 0);
//		buffer.writeInt32(0xFF00);
//		buffer.writeInt8(0);
//		
//		buffer.writeInt16((0 << 8) | (255 & 0xFF));
//		buffer.writeInt32(0xFF00000);
//		buffer.writeInt8(0);
//		
//		buffer.writeInt16((255 << 8) | (255 & 0xFF));
//		buffer.writeInt32(0xFF00FF);
//		buffer.writeInt8(0);
//		
//		buffer.writeInt16((200 << 8) | (200 & 0xFF));
//		buffer.writeInt32(0xFF00FF);
//		buffer.writeInt8(0);
//		
//		buffer.export("map_index2");
		
		frame.Map.load();
		new Frame();
	}

}
