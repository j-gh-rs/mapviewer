package shared.io;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.zip.GZIPInputStream;

public class Buffer {

	protected byte data[];
	private int position;
	private int bitPosition;
	private int save;

	public Buffer() {
		this(500);
	}

	public Buffer(int length) {
		this(new byte[length]);
	}

	public Buffer(byte[] data) {
		this.data = data;
		position = 0;
		save = 0;
	}

	public Buffer(File file) throws IOException {
		this(Files.readAllBytes(file.toPath()));
	}

	public void writeOp(int value) {
		writeInt8(value);
	}

	public void writeInt8(int value) {
		data[position++] = (byte)value;
	}

	public void writeInt16(int value) {
		data[position++] = (byte) (value >> 8);
		data[position++] = (byte) value;
	}

	public void writeInt24(int value) {
		data[position++] = (byte) (value >> 16);
		data[position++] = (byte) (value >> 8);
		data[position++] = (byte) value;
	}

	public void writeInt32(int value) {
		data[position++] = (byte) (value >> 24);
		data[position++] = (byte) (value >> 16);
		data[position++] = (byte) (value >> 8);
		data[position++] = (byte) value;
	}

	public void writeInt64(long value) {
		data[position++] = (byte) (value >> 56);
		data[position++] = (byte) (value >> 48);
		data[position++] = (byte) (value >> 40);
		data[position++] = (byte) (value >> 32);
		data[position++] = (byte) (value >> 24);
		data[position++] = (byte) (value >> 16);
		data[position++] = (byte) (value >> 8);
		data[position++] = (byte) value;
	}
	
	public void writeUInt8(int value) {
		writeInt8(value);
	}

	public void writeUInt16(int value) {
		writeInt16(value);
	}

	public void writeUInt24(int value) {
		writeInt24(value);
	}

	public void writeUInt32(int value) {
		writeInt32(value);
	}

	public void writeUInt64(long value) {
		writeInt64(value);
	}

	public byte readInt8() {
		return data[position++];
	}

	public int readInt16() {
		position += 2;
		return ((data[position - 2] << 8)
				| data[position - 1] & 0xFF);
	}

	public int readInt24() {
		position += 3;
		return (data[position - 3]) << 16
				| (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}

	public int readInt32() {
		position += 4;
		return (data[position - 4]) << 24
				| (data[position - 3] & 0xFF) << 16
				| (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}

	public long readInt64() {
		position += 8;
		return (data[position - 8]) << 56
				| (data[position - 7] & 0xFF) << 48
				| (data[position - 6] & 0xFF) << 40
				| (data[position - 5] & 0xFF) << 32
				| (data[position - 4] & 0xFF) << 24
				| (data[position - 3] & 0xFF) << 16
				| (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}

	public int readUInt8() {
		return (data[position++] & 0xFF);
	}

	public int readUInt16() {
		position += 2;
		return (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}
	
	public int readUInt24() {
		position += 3;
		return (data[position - 3] & 0xFF) << 16
				| (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}

	public int readUInt32() {
		position += 4;
		return (data[position - 4] & 0xFF) << 24
				| (data[position - 3] & 0xFF) << 16
				| (data[position - 2] & 0xFF) << 8
				| (data[position - 1] & 0xFF);
	}

	public long readUInt64() {
		position += 8;
		return (data[position - 8] & 0xFFL) << 56
				| (data[position - 7] & 0xFFL) << 48
				| (data[position - 6] & 0xFFL) << 40
				| (data[position - 5] & 0xFFL) << 32
				| (data[position - 4] & 0xFFL) << 24
				| (data[position - 3] & 0xFFL) << 16
				| (data[position - 2] & 0xFFL) << 8
				| (data[position - 1] & 0xFFL);
	}
	
	/** Reads one byte if first bit is 0, else reads two bytes, ignoring the first bit */
	public int getUSmart() {
	    int i = readUInt8();
	    if ((i & 0b10000000) != 0) {
	        i &= 0b01111111;
	        i <<= 8;
	        i |= readUInt8();
	    }
	    return i;
	}

//	public byte[] readBytes(byte endByte) {
//		int startPosition = position;
//		while (data[position++] != endByte);
//		byte[] b = new byte[position - startPosition - 1];
//		System.arraycopy(data, startPosition, b, 0, b.length);
//		return b;
//	}

	public void write(byte[] value) {
		write(value, 0, value.length);
	}

	public void read(byte[] value) {
		read(value, 0, value.length);
	}

	public void write(byte[] value, int offset, int length) {
		System.arraycopy(value, offset, data, position, length);
		position += length;
	}

	public void read(byte[] value, int offset, int length) {
		System.arraycopy(data, position, value, offset, length);
		position += length;
	}

	public void write(Buffer value) {
		write(value.data, 0, value.position);
	}

	public void writeString(String text) {
		write(text.getBytes());
		writeInt8(10);
	}

	public String readString(byte endByte) {
		int startPosition = position;
		while(data[position++] != endByte);
		return new String(data, startPosition, position - startPosition - 1);
	}
	
	public String readString() {
		return readString((byte) 10);
	}

	public void save() {
		save = position;
	}
	
	public int getSave() {
		return save;
	}

	public void load() {
		position = save;
	}

	public void reset() {
		position = 0;
	}

	public int position() {
		return position;
	}
	
	public int bitPosition() {
		return bitPosition;
	}

	public int length() {
		return data.length;
	}

	public boolean isEmpty() {
		return position == 0;
	}
	
	public int remaining() {
		return data.length - position;
	}

	public byte[] data() {
		return data;
	}

	public void seek(int position) {
		this.position = position;
	}

	public void skip(int i) {
		position += i;
	}

	public void initBitAccess() {
		bitPosition = position * 8;
	}

	public void finishBitAccess() {
		position = (bitPosition + 7) / 8;
	}

	public void writeBits(int amount, int value) {
		int byteOffset = bitPosition >> 3;
		int bitOffset = 8 - (bitPosition & 7);
		bitPosition += amount;
		
		if (amount <= bitOffset) {
			data[byteOffset] |= (value & BIT_MASKS[amount]) << bitOffset-amount;
		} else {
			data[byteOffset++] |= (value >>> amount - bitOffset) & BIT_MASKS[bitOffset];
			amount -= bitOffset;
			while(amount > 8) {
				data[byteOffset++] = (byte)(value >>> (amount - 8));
				amount -= 8;
			}
			data[byteOffset] |= (value & BIT_MASKS[amount]) << 8 - amount;
		}
	}

	public int readBits(int amount) {
		int byteOffset = bitPosition >> 3;
		int bitOffset = 8 - (bitPosition & 7);
		bitPosition += amount;
		
		if (amount <= bitOffset)
			return (data[byteOffset] & (BIT_MASKS[bitOffset] ^ BIT_MASKS[bitOffset-amount])) >>> bitOffset-amount;
		
		int value = data[byteOffset++] & BIT_MASKS[bitOffset];
		amount -= bitOffset;
		while(amount > 8) {
			value <<= 8;
			value |= data[byteOffset++] & 0xFF;
			amount -= 8;
		}
		value <<= amount;
		value |= ((data[byteOffset] >>> 8-amount) & BIT_MASKS[amount]);
		
		return value;
	}
	
	public void writePacketLength() {
		data[save - 1] = (byte)(position - save);
	}

	public void export(String path) {
		try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(path)))) {
			byte[] subdata = new byte[position];
			System.arraycopy(data, 0, subdata, 0, position);
			bos.write(subdata);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Buffer decompress(File file) throws IOException {
		GZIPInputStream gzis = new GZIPInputStream(new ByteArrayInputStream(Files.readAllBytes(file.toPath())));
		Buffer buffer = new Buffer(0x10000);
		
		byte[] b = new byte[0x400];
		int len = 0;

		while ((len = gzis.read(b)) > 0) {
		    buffer.write(b, 0, len);
		}
		
		buffer.reset();
		
		return buffer;
	}

	public static final int BIT_MASKS[] = {	0x00000000, 0x00000001, 0x00000003, 0x00000007,
										0x0000000F, 0x0000001F, 0x0000003F, 0x0000007F,
										0x000000FF, 0x000001FF, 0x000003FF, 0x000007FF,
										0x00000FFF, 0x00001FFF, 0x00003FFF, 0x00007FFF,
										0x0000FFFF, 0x0001FFFF, 0x0003FFFF, 0x0007FFFF,
										0x000FFFFF, 0x001FFFFF, 0x003FFFFF, 0x007FFFFF,
										0x00FFFFFF, 0x01FFFFFF, 0x03FFFFFF, 0x07FFFFFF,
										0x0FFFFFFF, 0x1FFFFFFF, 0x3FFFFFFF, 0x7FFFFFFF,
										0xFFFFFFFF };
}