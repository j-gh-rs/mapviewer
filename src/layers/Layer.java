package layers;

import tree.QuadTree;
import gridpane.Canvas;
import gridpane.GridPane;
import gridpane.ImageTile;
import tree.Node;

public abstract class Layer {
	
	private boolean visible = true;
	
	private final QuadTree<ImageTile> tree = new QuadTree<ImageTile>();
	
	public Layer() {
		
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public void draw(Canvas canvas, int scale, int x, int y, int iMin, int iMax, int jMin, int jMax) {
		if (!isVisible())
			return;
		
		/** Determine true tile index bounds */
		iMin = Math.max(0, iMin);
		jMin = Math.max(0, jMin);
		iMax = Math.min((1 << scale) - 1, iMax);
		jMax = Math.min((1 << scale) - 1, jMax);
		
//		Node<Tile> node = tree.get(scale, iMin, jMin);
//		Tile tile = node.get();
//		if (tile == null) {
//			tile = createTile(scale, iMin, jMin);
//			node.set(tile);
//		}
		
////		drawTile(canvas, tile);
//		for (int i = iMin; i <= iMax; i++)
//			for (int j = jMin; j <= jMax; j++)
//				canvas.draw(tile, x + j*256, y + i*256);
		

		for (int i = iMin; i <= iMax; i++) {
			for (int j = jMin; j <= jMax; j++) {
				Node<ImageTile> node = tree.get(scale, i, j);
				ImageTile imageTile = node.get();
				if (imageTile == null) {
					imageTile = createTile(scale, i, j);
					node.set(imageTile);
				}
				canvas.draw(imageTile, x + j * GridPane.TILE_SIZE, y - i * GridPane.TILE_SIZE);
			}
		}
	}
	
//	public static void drawTile(Canvas canvas, Tile tile, int x0, int y0) {
//		int x0c = Math.max(0, x0);
//		int y0c = Math.max(0, y0);
//		int dx = x0c - x0;
//
//		int idx = ((y0c - y0) << 8) | dx;
//		for (int y = 0; y < 256; y++) {
//			for (int x = x0c; x < x0c + dx; x++) {
//				canvas.pixels[y * canvas.width + x] = tile.data[idx++];
//			}
//		}
//		
//		for (int y = 0; y < 256; y++) {
//			for (int x = 0; x < 256; x++) {
//				canvas.pixels[y * canvas.width + x] = tile.data[(y << 8) | x];
//			}
//		}
//	}
	
	public abstract ImageTile createTile(int scale, int i, int j);
	
	

}
