package layers.region;

import java.io.File;
import java.io.IOException;

import main.Frame;
import shared.io.Buffer;

public class Region {
	
	public static final int TILE_COUNT = 64;
	
	public static final int TILE_INACCESSIBLE = 1 << 21;
	
	public static Region[] regions;

	final int i;
	final int j;
	public final int mapGroundFileId;
	public final int mapObjectsFileId;
	final boolean isMembers;
	public final int[][][] clip = new int[4][TILE_COUNT][TILE_COUNT];

	public Region(int id, int mapGroundFileId, int mapObjectsFileId, boolean isMembers) {
		this.i = id & 0xFF;
		this.j = id >> 8;
		this.mapGroundFileId = mapGroundFileId;
		this.mapObjectsFileId = mapObjectsFileId;
		this.isMembers = isMembers;
		
		System.out.println("New region at i: "+i+", j: "+j);
	}

	public Region(Buffer indexBuffer) throws IOException {
		int id = indexBuffer.readUInt16();
		this.i = id & 0xFF;
		this.j = id >> 8;
		mapGroundFileId = indexBuffer.readUInt16();
		mapObjectsFileId = indexBuffer.readUInt16();
		isMembers = indexBuffer.readUInt8() == 0;

		Buffer objectBuffer = Buffer.decompress(new File(Frame.DATA_PATH, "world/map/" + mapObjectsFileId + ".gz"));
		Buffer groundBuffer = Buffer.decompress(new File(Frame.DATA_PATH, "world/map/" + mapGroundFileId + ".gz"));
		
		if (objectBuffer == null || groundBuffer == null) {
			new Exception().printStackTrace();
			return;
		}
		
		int[][][] someArray = new int[4][TILE_COUNT][TILE_COUNT];
		
		for (int p = 0; p < 4; p++) {
			for (int j = 0; j < TILE_COUNT; j++) {
				for (int i = 0; i < TILE_COUNT; i++) {
					while (true) {
						int v = groundBuffer.readUInt8();
						if (v == 0) {
							break;
						} else if (v == 1) {
							groundBuffer.skip(1);
							break;
						} else if (v <= 49) {
							groundBuffer.skip(1);
						} else if (v <= 81) {
							someArray[p][j][i] = v - 49;
						}
					}
				}
			}
		}
		
		for (int p = 0; p < 4; p++) {
			for (int j = 0; j < TILE_COUNT; j++) {
				for (int i = 0; i < TILE_COUNT; i++) {
					if ((someArray[p][j][i] & 0b01) != 0) {
						int height = p;
						if ((someArray[1][j][i] & 0b10) != 0) {
							height--;
						}
						if (height >= 0 && height <= 3) {
							clip[height][j][i] |= TILE_INACCESSIBLE;
						}
					}
				}
			}
		}
		
		int objectId = -1;
		int incr;
		while ((incr = objectBuffer.getUSmart()) != 0) {
			objectId += incr;
			int location = 0;
			int incr2;
			while ((incr2 = objectBuffer.getUSmart()) != 0) {
				location += incr2 - 1;
				int localX = location >> 6 & 0x3f;
				int localY = location & 0x3f;
				int height = location >> 12;
				int objectData = objectBuffer.readUInt8();
				int type = objectData >> 2;
				int direction = objectData & 0x3;
				if (localX < 0 || localX >= TILE_COUNT || localY < 0 || localY >= TILE_COUNT) {
					continue;
				}
				if ((someArray[1][localX][localY] & 2) == 2) {
					height--;
				}
				if (height >= 0 && height <= 3) {
//					addObject(objectId, absX + localX, absY + localY, height,
//							type, direction, false);
				}
			}
		}
	}

	public static void load() {
		try {
			Buffer indexBuffer = new Buffer(new File(Frame.DATA_PATH, "world/map_index"));
			int regionCount = indexBuffer.length() / 7; /** 7 bytes per region: id(2),groundFileId(2),objectFileId(2),memberFlag(1) */
			regions = new Region[regionCount];
			for (int i = 0; i < regionCount; i++) {
				regions[i] = new Region(indexBuffer);
			}
			
			System.out.println(regionCount + " regions loaded.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Region get(int i, int j) {
		for (Region region : regions)
			if (region.i == i && region.j == j)
				return region;
		return null;
	}

}
