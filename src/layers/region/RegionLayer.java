package layers.region;

import gridpane.GridPane;
import gridpane.ImageTile;
import layers.Layer;

public class RegionLayer extends Layer {
	
	public final int WIDTH = 30;
	public final int HEIGHT = 22;
	public final static int SCALE1 = 8;

	@Override
	public ImageTile createTile(int scale, int i, int j) {
		System.out.println("Creating tile scale: "+scale+", i: "+i+", j: "+j);
		ImageTile imageTile = new ImageTile();
		
		if (scale > SCALE1) {
			Region region = Region.get(i >> (scale - SCALE1), j >> (scale - SCALE1));
			if (region == null)
				return ImageTile.EMPTY_TILE;
			imageTile.drawRect(0, 0, GridPane.TILE_SIZE, GridPane.TILE_SIZE, region.isMembers ? 0xFF : 0xFF00);
		} else {
			boolean empty = true;

			int size = 1 << scale;
			for (int ii = i << (SCALE1 - scale); ii < (i+1) << (SCALE1 - scale); ii++) {
				for (int jj = j << (SCALE1 - scale); jj < (j+1) << (SCALE1 - scale); jj++) {
					Region region = Region.get(ii, jj);
					if (region == null)
						continue;
//					tile.fillRect((jj << scale) % 256,  GridPane.TILE_SIZE - size - ((ii << scale) % 256), size, size, (region.mapGroundFileId << 16) | region.mapObjectsFileId);
					imageTile.drawRect((jj << scale) % 256,  GridPane.TILE_SIZE - size - ((ii << scale) % 256), size, size, region.isMembers ? 0xFF : 0xFF00);
					empty = false;
				}
			}
			
			return empty ? ImageTile.EMPTY_TILE : imageTile;
		}
		
		return imageTile;
	}
}
