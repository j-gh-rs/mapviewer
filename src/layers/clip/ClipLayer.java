package layers.clip;

import java.awt.Color;

import gridpane.GridPane;
import gridpane.ImageTile;
import layers.Layer;
import layers.region.Region;
import layers.region.RegionLayer;

public class ClipLayer extends Layer {

	public final static int SCALE0 = 6;
	public final static int SCALE1 = SCALE0 + 8;

	@Override
	public ImageTile createTile(int scale, int i, int j) {
		System.out.println("ClipLayer: Creating tile scale: "+scale+", i: "+i+", j: "+j);
		ImageTile imageTile = new ImageTile();
		
		if (scale < SCALE0) { /** Tile smaller than 1x1 px, deemed out of scope */
			return ImageTile.EMPTY_TILE;
		} else if (scale >= SCALE0 && scale < RegionLayer.SCALE1) { /** Tile covers nxn px, where n = 1 << (scale - SCALE0) */
			boolean empty = true;

			int regionSize = 1 << (6+scale-SCALE0);
			int tileSize = 1 << (scale-SCALE0);
			for (int iRegion = i << (RegionLayer.SCALE1 - scale); iRegion < (i+1) << (RegionLayer.SCALE1 - scale); iRegion++) {
				for (int jRegion = j << (RegionLayer.SCALE1 - scale); jRegion < (j+1) << (RegionLayer.SCALE1 - scale); jRegion++) {
					Region region = Region.get(iRegion, jRegion);
					if (region == null)
						continue;

					for (int iTile = 0; iTile < 64; iTile++) {
						for (int jTile = 0; jTile < 64; jTile++) {
							imageTile.fillRect((jRegion * regionSize) % 256 + (jTile << (scale-SCALE0)), GridPane.TILE_SIZE - tileSize - (iRegion * regionSize) % 256 - ((iTile << (scale-SCALE0))), tileSize, tileSize, region.clip[0][jTile][iTile]);
						}
					}
					empty = false;
				}
			}
			
			return empty ? ImageTile.EMPTY_TILE : imageTile;
		} else if (scale >= RegionLayer.SCALE1 && scale <= SCALE1) { /** Region spans 1 or multiple ImageTiles */
			Region region = Region.get(i >> (scale - RegionLayer.SCALE1), j >> (scale - RegionLayer.SCALE1));
			if (region == null)
				return ImageTile.EMPTY_TILE;
			
			int tileSize = 1 << (scale-SCALE0);
			int tileCount = 1 << (8-scale+SCALE0);
			
			int iTile0 = (i & ((1<<(scale - RegionLayer.SCALE1))-1)) * tileCount;// / (1<<(scale - RegionLayer.SCALE1));
			int jTile0 = (j & ((1<<(scale - RegionLayer.SCALE1))-1)) * tileCount;// / (1<<(scale - RegionLayer.SCALE1));

			for (int iTile = iTile0; iTile < iTile0 + tileCount; iTile++) {
				for (int jTile = jTile0; jTile < jTile0 + tileCount; jTile++) {
					int x0 = ((jTile-jTile0) << (scale-SCALE0));
					int y0 = GridPane.TILE_SIZE - tileSize - (((iTile-iTile0) << (scale-SCALE0)));
					imageTile.fillRect(x0, y0, tileSize, tileSize, region.clip[0][jTile][iTile]);
					int clip = region.clip[0][jTile][iTile];
					if (clip != 0 && clip != 2097152)
						System.out.println(clip);
					if ((clip & 0b10000000) != 0)
						imageTile.drawVerticalLine(5, y0, tileSize, 0xFFFFFF);
					if ((clip & 0b00100000) != 0)
						imageTile.drawHorizontalLine(x0, 5, tileSize, 0xFFFFFF);
					if ((clip & 0b00001000) != 0)
						imageTile.drawVerticalLine(tileSize-5, y0, tileSize, 0xFFFFFF);
					if ((clip & 0b00000010) != 0)
						imageTile.drawHorizontalLine(x0, tileSize-5, tileSize, 0xFFFFFF);
				}
			}
			
			return imageTile;
		}
		
		return imageTile;
	}

}
